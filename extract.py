__author__ = 'Josh Riojas'

import re, mmap

pat_infoLink = re.compile(b'<div class=\"float-left product-thumbnail toggle\".*\s*<a href="(.*)">\s*<noscript>(<img src.*?\/>)')
pat_title = re.compile(b'(<div class=\"product-line unseen\" nid=\")(.*) title="(.*?)">')
count = 1

titles = list()
links = list()
thumbnails = list()

books = dict()

filename = input('Enter filename: ')

with open(filename, 'r+') as inFile:
    data = mmap.mmap(inFile.fileno(), 0)
    matchTitles = re.findall(pat_title, data)
    matchInfo = re.findall(pat_infoLink, data)

    for it in range (0, len(matchTitles)):
        books[matchTitles[it][2].decode("utf-8")] = (matchInfo[it][0].decode("utf-8"), matchInfo[it][1].decode("utf-8"))
        #titles.append(matchTitles[it][2].decode("utf-8"))
        #links.append(matchInfo[it][0].decode("utf-8"))
        #thumbnails.append(matchInfo[it][1].decore("utf-8"))

titles = sorted(books.keys(), key=str.lower)

with open("booklist.html", 'w') as outFile:
    print("""<doctype html>
<head>
	<title>Packt Publishing Library</title>
	<style type="text/css">
	<!--
	html, body {
		font-size: 16px;
		line-height: 1em;
		margin: 0;
		padding: 0;
	}
	body {
		background-color: #CCC;
	}
	#content {
	  margin-top: 5px;
  	}
	.book {
		background-color: #FFF;
		border: 1px solid #EFEFEF;
		border-bottom: 3px solid #2D2D2D;
		width: 600px;
		height: 130px;
		margin: 0 auto;
	}
	.book:first-child {
		border-radius: 5px 5px 0 0;
	}
	.book:last-child {
	  border-radius: 0 0 5px 5px;
	  border-bottom: 1px solid #EFEFEF;
	  margin-bottom: 5px;
  	}
	.book img {
		float: left;
		margin: 7px 0 0 10px;
	}
	.info {
		float: left;
		margin-left: 10px;
	}
	.title {
		font-weight: bold;
		width: 480px;
		word-wrap: break-word;
	}
	a {
		color: #BC6600;
	}
	-->
	</style>
</head>

<body>
	<article id="content">""", file=outFile)

    for it in range (0, len(titles)):
        print('\n\t\t<section class="book">\n\t\t\t<img src="http:' + books.get(titles[it])[1][10:]
              + '\n\n\t\t\t<div class="info">\n\t\t\t\t<p class="title">' + titles[it] + '</p>\n\t\t\t\t'
              + '<p>View book details &raquo; <a href="https://www.packtpub.com' + books.get(titles[it])[0]
              + '" target="_blank">External Link</a></p>\n\t\t\t</div>\n\t\t</section>', file=outFile)
